import os

TG_API_TOKEN = os.getenv('TG_API_TOKEN')
DEBUG = True
HOST = '0.0.0.0'
PORT = '5000'
MAIN_CONTAINER_URL = 'http://apimyhistory/api'
